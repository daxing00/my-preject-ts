import { Switch, Route, Redirect } from '@jupiter/plugin-runtime/router';
import { ReactComponent as Logo } from './Logo.svg';
import routerArray from './router/routerArray';
import './App.css';
import './App.less';
import RouterNav from './component/routerNav';

const App: React.FC = () => (
  <div>
    <RouterNav />
    <Switch>
      <Redirect exact={true} path="/" to="dataType" />
      {routerArray.map(({ path, component }) => (
        <Route key={path} path={path} component={component} />
      ))}
      <Route path="*">
        <div>404</div>
      </Route>
    </Switch>
  </div>
);

export default App;
