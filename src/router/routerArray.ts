import React from 'react';
import DataType from '../views/dataType';
import Fun from '../views/fun';
import Class from '../views/class';

export interface RouterArrayType {
  path: string;
  name: string;
  component: any;
}

const routerArray: Array<RouterArrayType> = [
  { path: '/dataType', name: '数据类型', component: DataType },
  { path: '/fun', name: '函数', component: Fun },
  { path: '/class', name: '类', component: Class },
];

export default routerArray;
