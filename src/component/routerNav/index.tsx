import { NavLink } from '@jupiter/plugin-runtime/router';
import React from 'react';
import routerArray from '@/router/routerArray';

export default function (): React.ReactElement {
  return (
    <div>
      {routerArray.map(({ path, name }) => (
        <NavLink to={path} key={path}>
          {name}
        </NavLink>
      ))}
    </div>
  );
}
